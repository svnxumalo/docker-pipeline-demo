# Project Docker Pipeline Demo

# Dockerfile

To ensure testing right up to the deployment of the solution please ensure you have the below tools installed and ready to go:

- Docker
- Terraform
- kubectl
- kubergrunt

## Building the Docker Image

```
docker build -t docker_pipeline_demo .
```

## Running the application Locally Using Docker

```
docker run -it --rm -p 127.0.0.1:8083:8083 --name docker_pipeline_demo docker_pipeline_demo
```

# Application AWS EKS Deployment Using Terraform 

## Pre-Setup

Make sure you have terraform installed, because I am using Gitlab as my repository I will be saving terraforms backend on GitLab.

### Initialize Terraform Remote Backend

```
terraform init \                        ✔  docker-pipeline-demo  
    -backend-config="address=https://gitlab.com/api/v4/projects/${gitlab_PROJECT_ID}/terraform/state/${gitlab_STATE_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${gitlab_PROJECT_ID}/terraform/state/${gitlab_STATE_NAME}/lock" \ 
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${gitlab_PROJECT_ID}/terraform/state/${gitlab_STATE_NAME}/lock" \
    -backend-config="username=${gitlab_USERNAME}" \
    -backend-config="password=${gitlab_API_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

### Set Environment Variables

```
export gitlab_USERNAME=""
export gitlab_API_TOKEN=""
export gitlab_PROJECT_ID=""
export gitlab_STATE_NAME=""
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
export TF_VAR_cluster_name=""
```
### Terraform Plan Deployment

To run a dry run on the infrastructure you would like to deploy run the command below:

```
terraform plan
```

### Terraform Deploy Infrastructure

To Deploy your AWS EKS infrastructure to AWS run: 

```
terraform apply
```

### Kubernetes Deployment
Once the terraform commands above have completed successufully, its time to deploy your kubernetes services to the AWS EKS cluster you just deployed.

#### Connect to the cluster 
```
aws eks update-kubeconfig --name docker_pipeline_demo --region eu-west-1
```

### Modify the ```aws-auth.yaml``` 

If you would like to grant users of groups access to the cluster uing IAM permisions. Once modified run the command below to setup the ConFigMap

```
kubectl apply -f auth-auth.yaml
```

#### Now you are ready to Deploy Services

```
kubectl apply -f service.yaml  
```

```
kubectl apply -f pod.yaml
```

### To connect to your EKS Deployed Services

run the command below to get the publicly accesable address:

```
kubectl get services 
```

**N.B.** To connect to the application you have just deployed observe the output of the above command that you just ran

## Cleanup

Remove all deployed kubernetes services
```
kubectl delete all alll
```

Remove all terraform deployed resources

```
terraform destroy
```

# Task

## Dockerise the application
- [x] Create a free GitHub / GitLab / BitBucket account using any name (or use your existing personal account)
- [x] Create a public repository called docker-pipeline-demo (GitLab or Github)
- [x] Add a Docker file to allow a Docker image to be created for the application
- [x] Add a README.md file that explains how to build and run the container locally

## Optional
- [x] Setup a build / publish pipeline to build the images on every commit (both Gitlab and Github Actions provides free tiers)
- [x] As a part of the pipeline, push the built docker image to a docker registry automatically (both Github and Docker provides free accounts)
- [x] Add a Kubernetes Deployment to the repo and document the steps needed to deploy the application (use README.md)