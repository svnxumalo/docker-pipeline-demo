FROM python:3.10.12-alpine3.18

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8083

CMD [ "python", "./app.py" ]